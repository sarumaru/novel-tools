<?php

function num2kanji($num)
{
	$number = ['零','一','二','三','四','五','六','七','八','九'];
	$unit   = ['','十','百','千','万','億'];

	for ($r ='', $i = 0; ; $i++) {
		$r = ((0 == $i && (0 == $num || 0 < $num % 10)) ||
		      (0 <  $i &&  1 < $num % 10) ? $number[$num % 10] : '')
		   . (0 < $num % 10 ? $unit[$i] : '')
		   . $r;
		$num = intval($num / 10);
		if (0 == $num) {
			break;
		}
	}

	return $r;
}

//for ($i = 0; $i < 200; $i++) {echo num2kanji($i) . ' ';}exit;

define('LINE_BREAK', "\n");

mb_internal_encoding('UTF-8');

$input = isset($argv[1]) ? $argv[1] : '';
$output= isset($argv[2]) ? $argv[2] : $input;

if (empty($input))
{
	echo sprintf('usage: numbering-sections.php INPUT');
	exit;
}

$src = @ file_get_contents($input);
$src = str_replace("\r", "\n", str_replace("\r\n", "\n", $src));

$dst = '';

$section_no = 1;

foreach (explode("\n", $src) as $line)
{
	if (preg_match('/^(［＃(.+?)見出し］)(.+?)(［＃.+)/u', $line, $m))
	{
		$m[3] = preg_replace('/第.+?話[　 \t]/u', '', $m[3]); // 一度削除し
		$m[3] = '第' . num2kanji($section_no) . '話　' . $m[3]; // 付け直す
//echo $m[3] . PHP_EOL;
		$line = $m[1] . $m[3] . $m[4];
		$section_no++;
	}
	
	$dst .= $line . LINE_BREAK;
}

$dst = trim($dst);

if ($src != $dst)
{
	file_put_contents($output, $dst);
}
