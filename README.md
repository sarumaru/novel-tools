# 小説書くのに利用しているツール類

## これはなに？

私が小説書くのに利用しているツール類。

随時更新。

## それぞれ

|ファイル名|概要|
|-|-|
|`split-sections.php`|章ごとの分割/結合ツール|
|`count-sections.php`|章ごとの文字数カウントツール|
|`numbering-sections.php`|章番号を漢数字で設定するツール|
|`normalize.php`|文章正規化ツール|
|`otherworldly_language.php`|日本語 to 異世界語 単換字式暗号 変換 ツール|

## ライセンス

Copyright (c) 2016 Shun Sarumaru.

このソフトウエアは [The MIT License](https://opensource.org/licenses/mit-license.php) の下で提供されています。
