<?php

/*
 * 参考：
 *   http://www.aozora.gr.jp/KOSAKU/MANUAL_2.html
 *   https://ja.wikipedia.org/wiki/%E5%BC%95%E7%94%A8%E7%AC%A6
 *   http://novelgakuen.com/suuji.html
 *   http://ncode.syosetu.com/n4623bk/10/
 *   http://novelu.com/suuji/
 *   http://matome.naver.jp/odai/2137856189375688101
 */

define('LINE_BREAK', "\n");

define('WORDS_TYPE_SERIF', 'serif'); // 会話文
define('WORDS_TYPE_RAW',   'raw'); // 地の文
define('WORDS_TYPE_DESC',  'desc'); // 説明文
define('WORDS_TYPE_LIST',  'list'); // リスト
define('WORDS_TYPE_CAPTION', 'caption'); // 見出し

mb_internal_encoding('UTF-8');

$input = isset($argv[1]) ? $argv[1] : '';
$output= isset($argv[2]) ? $argv[2] : $input;

if (empty($input))
{
	echo sprintf('usage: normalize.php INPUT [OUTPUT]');
	exit;
}

$src = @ file_get_contents($input);
$dst = '';

$src = str_replace("\r", "\n", str_replace("\r\n", "\n", $src));

$words_type_cur = WORDS_TYPE_RAW;
$words_type_old = WORDS_TYPE_RAW;

$line_no = 0;
foreach (explode("\n", $src) as $line)
{
	$line_no++; // continue 対策のため頭でインクリ

	if (preg_match ('/^$/u', $line))
	{ // 空行は飛ばす
		continue;
	}
	else if (preg_match ('/^ +$/u', $line))
	{ // 擬似空行は飛ばす
		$dst .= ' ' . LINE_BREAK;
		continue;
	}

	$line = preg_replace_callback('/(《.+?》)/u', function ($s) {
					return preg_replace('/(　| |\t)/', "\x7F", $s[1]);
				}, $line);

	// 読点の連続はリーダ記号に
	$line = preg_replace('/(、){2,}/u', '…', $line);

	// リーダ記号の数を統一
	$line = preg_replace('/…+/u', '……', $line);

	// 長音の連続はダッシュ記号に
	$line = preg_replace('/(ー){2,}/u', '―', $line);

	// ダッシュ記号の数を統一
	$line = preg_replace('/―+/u', '――', $line);

	// ダブルコーテーション、引用符を統一
	$line = preg_replace('/["“”](.+?)["“”]/u', '“$1”', $line);

	// 感嘆符、疑問符の直後は１文字開ける
	$line = preg_replace('/([！？!\?]+)/u', '$1　', $line);

	// 半角を全角に
	$line = preg_replace('/\(/u',   '（', $line);
	$line = preg_replace('/\)/u',   '）', $line);
	$line = preg_replace('/,/u',    '、', $line);
	$line = preg_replace('/\./u',   '。', $line);
	$line = preg_replace('/！？/u', '!?', $line);
	$line = preg_replace('/？！/u', '!?', $line);

	// 数字
	$line = preg_replace('/？！/u', '!?', $line);
	$line = preg_replace_callback('/([0-9０１２３４５６７８９]+)/u', function ($s) {
					return 2 == mb_strlen($s[1]) ? mb_convert_kana($s[1], 'n')  // 半角に
					                             : mb_convert_kana($s[1], 'N'); // 全角に
				}, $line);

	// 会話文末尾の空白や句読点を削除
	$line = preg_replace('/(、|。)(」|』|）|》)/u', '$2', $line);
	$line = preg_replace('/　+(」|』|）|《|》)/u', '$1', $line);

	// 行頭、行末の空白文字を削除
	$line = preg_replace('/^(　| |\t)/u', '', $line);
	$line = preg_replace('/(　| |\t)$/u', '', $line);

	// 水平線の数を統一
	$line = preg_replace('/^-{5,}/u', str_pad('', 70, '-'), $line);

	// ルビの垂直線を統一
	$line = preg_replace('/[│]([^｜]+?)《(.+?)》/u', '｜$1《$2》', $line);

	// 行内の空白文字を削除
//	$line = preg_replace('/(　| |\t)/u', '', $line);
	$line = preg_replace('/(　)+/u', '$1', $line);
	$line = preg_replace('/( )+/u',  '$1', $line);
	$line = preg_replace('/(\t)+/u', '$1', $line);

	$line = preg_replace_callback('/(《.+?》)/u', function ($s) {
					return preg_replace("/\x7F+/", ' ', $s[1]);
				}, $line);

	if (preg_match_all('/[｜\|](.*?)《(.+?)》/u', $line, $m))
	{
		for ($i = 0; $i < count($m[0]); $i++) {
			// ルビチェック
			if (10 < mb_strlen($m[2][$i], 'utf-8')) {
				// http://syosetu.com/man/ruby/
				// 「小説家になろう」
				//   ルビの仕様
				//   > ルビ部分は10文字以内で入力してください。
				//   > 11文字以上入力された場合はルビが解除されます。
				echo sprintf('%5d 行目: 「小説家になろう」 ルビ仕様逸脱 ”%s” ルビ "%s" %d 文字'
						, $line_no, $m[1][$i], $m[2][$i], mb_strlen($m[2][$i], 'utf-8')
						).PHP_EOL;
			}
			if (empty($m[1][$i])) {
				// http://syosetu.com/man/ruby/
				// 「小説家になろう」
				echo sprintf('%5d 行目: 「小説家になろう」 ルビ仕様逸脱 対象が空 ルビ "%s"'
						, $line_no, $m[2][$i], mb_strlen($m[2][$i], 'utf-8')
						).PHP_EOL;
			}
		}
	}

	// ベタの文章の開始/終了
	if (preg_match('/^-{5,}/', $line))
	{
		$dst .= WORDS_TYPE_DESC != $words_type_cur ? LINE_BREAK : '';
		$dst .= $line . LINE_BREAK;

		if (WORDS_TYPE_DESC == $words_type_cur)
		{
			$words_type_old = $words_type_cur;
			$words_type_cur = '';
		}
		else
		{
			$words_type_cur = WORDS_TYPE_DESC;
			$words_type_old = $words_type_cur;
		}

		continue;
	}

	// 行頭の会話文開始をマーク
	if (WORDS_TYPE_DESC != $words_type_cur)
	{
		if (preg_match('/^(「|『)/u', $line))
		{
			$words_type_cur = WORDS_TYPE_SERIF;
		}
		else if (preg_match('/^・/u', $line))
		{
			$words_type_cur = WORDS_TYPE_LIST;
		}
		else if (preg_match('/.*(［＃.見出し］.+?［＃.見出し終わり］).*/u', $line, $m))
		{
			$line = $m[1];
			$words_type_cur = WORDS_TYPE_CAPTION;
		}
		else
		{
			$words_type_cur = WORDS_TYPE_RAW;
		}
	}

	if ($words_type_cur != $words_type_old)
	{
		$dst .= LINE_BREAK;
	}

	switch ($words_type_cur)
	{
	case WORDS_TYPE_SERIF:// 会話文
	case WORDS_TYPE_DESC: // 説明文
	case WORDS_TYPE_LIST: // リスト
		$dst .= $line . LINE_BREAK;
		break;
	case WORDS_TYPE_CAPTION: // 見出し
		$dst .= $line . LINE_BREAK;
		break;
	case WORDS_TYPE_RAW: // 地の文
		// 地の文は行頭を１文字下げる
		$dst .= '　' . $line . LINE_BREAK;
		break;
	}

	$words_type_old = $words_type_cur;
}

$dst = trim($dst);

if ($src != $dst)
{
	file_put_contents($output, $dst);
}
