<?php

// http://php.net/manual/ja/function.str-split.php
function str_split_unicode($str, $l = 0) {
	if ($l > 0) {
		$ret = array();
		$len = mb_strlen($str, "UTF-8");
		for ($i = 0; $i < $len; $i += $l) {
			$ret[] = mb_substr($str, $i, $l, "UTF-8");
		}
		return $ret;
	}
	return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
}

if (!isset($argv[1]))
{
	echo sprintf('usage: php %s {TEXT}'.PHP_EOL, $argv[0]);
	exit;
}

$text = $argv[1];

//$tbl = "？！〆＋－±×÷＝≠∞∴♂♀￥＄￠￡％＃＆＊＠§☆★○●◎◇◆□■△▲▽▼※〓∀∂∇≡≒∝∵∫∬Å‰♯♭♪†‡¶ヰヱΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩβγδεζηθικλμνξοπρστυφχψωБГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";

//$conv_chars = 'ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡヰヱΣΤΥΦΧΨΩАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ♯♭¶∫∬〆∂∀∞￥＄￠￡％＃＆＊＠§☆○◇□△▽※';
$conv_chars = '□■△▲▽▼※〓☆★○●◎◇◆＄￠￡％＃＆＠☆〆□■△▲▽▼※〓☆★○●◎◇◆＄￠￡％＃＆＠☆〆□■△▲▽▼※〓☆★○●◎◇◆＄￠￡％＃＆＠☆〆□■△▲▽▼※〓☆★○●◎◇◆＄￠￡％＃＆＠☆〆□■△▲▽▼※〓☆★○●◎◇◆＄￠￡％＃＆＠☆〆';
//$conv_chars = 'あぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをん';
$conv_chars = 'アィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲン';
$conv_chars = str_split_unicode($conv_chars, 1);

$kana = 'ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲンヴヵヶ';

$over = count($conv_chars) * 0; // 元の文字を残す割合

for ($try = 10; 0 < $try; $try--)
{
	$tbl = array();
	for ($i = 0, $use = 0; $i < mb_strlen($kana, 'utf-8'); $i++)
	{
		$c = mb_substr($kana, $i, 1, 'utf-8');
		$index = mt_rand(0, count($conv_chars) + $over);
		if (count($conv_chars) <= $index ||
			count($conv_chars) <= $use) {
			$tbl[$c] = null;
		}
		else if ($index < count($conv_chars)) {
			$use++;
			$tbl[$c] = $conv_chars[$index];
		}
		else {
			$i--;
			continue;
		}
	}
	
	for ($i = 0, $n = mb_strlen($text, 'utf-8'); $i < $n; $i++)
	{
		$c = mb_substr($text, $i, 1, 'utf-8');
		echo (isset($tbl[$c]) ? $tbl[$c] : $c);
	}
	echo PHP_EOL;
}