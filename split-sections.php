<?php

function get_split_section_filename($basename, $section_no = -1)
{ // セクションを分割した時のファイル名を生成
	$p = pathinfo($basename);
	$path = sprintf('%s%s%s%s%s-%04d.%s'
		, $p['dirname'], DIRECTORY_SEPARATOR
		, $p['filename'], DIRECTORY_SEPARATOR
		, $p['filename'], $section_no, $p['extension']);
	return $path;
}

mb_internal_encoding('UTF-8');

define('LINE_BREAK', "\n");

$input = isset($argv[1]) ? $argv[1] : '';
$output= isset($argv[2]) ? $argv[2] : $input;

if (empty($input))
{
	echo sprintf('usage: split-sections.php INPUT');
	exit;
}

$src_timestamp = @ filemtime($input);
$dst_dir = dirname(get_split_section_filename($input));

if (false !== $src_timestamp)
{ // 分割処理
	echo 'split sections ...';
	//
	$src = @ file_get_contents($input);
	$src = str_replace("\r", "\n", str_replace("\r\n", "\n", $src));
	$src.= "\n" . '［＃大見出し］' . "\x7F" . '［＃大見出し終わり］'; // 番兵
	$dst = '';
	$sections = [];
	foreach (explode("\n", $src) as $line)
	{
		if (preg_match('/^(［＃(.+?)見出し］)(.+?)(［＃.+)/u', $line, $m))
		{
			if (!empty($dst)) {
				$path = get_split_section_filename($input, count($sections));
				$dst_timestamp = @ filemtime($path);
				if ($src_timestamp < $dst_timestamp) {
					echo PHP_EOL . 'Already new file exists: ' . basename($path) . PHP_EOL;
					exit;
				}
				$sections[] = trim($dst) . LINE_BREAK;
				$dst = '';
			}
		}
		$dst .= $line . LINE_BREAK;
	}
	// 保存先フォルダを生成
	@ mkdir($dst_dir);
	// セクションを分割
	foreach ($sections as $section_no => $section)
	{
		file_put_contents(get_split_section_filename($input, $section_no + 1), $section);
	}
	// 元ファイル削除
	unlink($input);
	//
	echo ' completed' . PHP_EOL;
	echo 'split to ' . count($sections) . ' sections' . PHP_EOL;
}
else
{ // 結合処理
	echo 'concat sections ...';
	$erases = [];
	$path = str_replace('-12345', '*', get_split_section_filename($input, -12345));
	$src = '';
	foreach (glob($path) as $fname) {
		if (filemtime($fname) < $src_timestamp) {
			echo PHP_EOL . 'Already new file exists: ' . basename($input) . PHP_EOL;
			exit;
		}
		$erases[] = $fname;
		$src .= trim(@ file_get_contents($fname)) . LINE_BREAK . LINE_BREAK;
	}
	// 一つに結合
	file_put_contents($input, $src);
	// 展開していたセクションを削除
	foreach ($erases as $fname) {
		unlink($fname);
	}
	@ unlink(dirname(get_split_section_filename($input)) . DIRECTORY_SEPARATOR . '.DS_Store');
	@ rmdir(dirname(get_split_section_filename($input)));
	//
	echo ' completed' . PHP_EOL;
	echo 'concat from ' . count($erases) . ' sections' . PHP_EOL;
}
