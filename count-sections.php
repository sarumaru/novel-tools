<?php

define('LINE_BREAK', "\n");

mb_internal_encoding('UTF-8');

$input = isset($argv[1]) ? $argv[1] : '';

if (empty($input))
{
	echo sprintf('usage: count-sections.php INPUT');
	exit;
}

$src = @ file_get_contents($input);
$src = str_replace("\r", "\n", str_replace("\r\n", "\n", $src));

$sections = [];

$section = [
	'name' => '',
	'level' => 0,
	'char' => 0,
	'line' => 0,
];

$line_no = 0;
foreach (explode("\n", $src) as $line)
{
	$line_no++; // continue 対策のため頭でインクリ

	if (preg_match ('/^［＃(.+?)見出し］(.+?)［＃/u', $line, $m))
	{
		if (!empty($section['name'])) {
			$sections[] = $section;
		}
			
		$char_count = 0;
		$line_count = 0;

		$section = [
			'name' => $m[2],
			'level' => strpos('大中小', $m[1]),
			'char' => 0,
			'line' => 0,
		];
	
		continue;
	}
	
	$section['char'] += mb_strlen(preg_replace('/　|-/', '', $line));
	$section['line']++;
}

$sections[] = $section;

//print_r($sections);
foreach ($sections as $section)
{
	echo sprintf('char:%5d: line:%4d:「%s」', $section['char'], $section['line'], $section['name']). PHP_EOL;
}
